public class article extends product
{
	private String author;
	private magazine content_of;
	private int page;

	public article(String param_title, String param_author, magazine param_content_of, int param_page)
        {
                super(param_title, type.ARTICLE);
                author = param_author;
		content_of = param_content_of;
		page = param_page;
        }

        public String toString()
        {
                return "ID: " + id + "; Titel: " + title + "; Autor: " + author + "; Seite: " + page + "; Zeitschrift: \n   " + content_of;
        }
}	
