public class magazine extends product
{
	private String edition;
	private String year;
	private String publisher;

        public magazine(String param_title, String param_edition, String param_year, String param_publisher)
        {
                super(param_title, type.MAGAZINE);
                edition = param_edition;
		year = param_year;
		publisher = param_publisher;
        }

        public String toString()
        {
                return "ID: " + id + "; Titel: " + title + "; Ausgabe: " + edition + "; Jahrgang: " + year + "; Verlag: " + publisher;
        }
}	
