public abstract class product
{
	private static int next_id = 0;
	protected int id;
	protected String title;
	private type type;

	public product(String param_title, type param_type)
	{
		id = next_id;
		next_id++;
		title = param_title;
		type = param_type;
	}
	
	public String gettitle()
	{
		return title;
	}
	public int getid()
	{
		return id;
	}

	public type gettype()
	{
		return type;
	}
}

