import java.util.*;

public class management
{
	private LinkedList<product> linked_list;


	public management()
	{
		linked_list = new LinkedList<product>();
	}

	public void add_product(product param_product)
	{
		linked_list.add(param_product);
	}

	public int add_book(String param_title, String param_author, String param_publication_date, String param_publisher, String param_ISBN)
	{
		book temp_book = new book(param_title, param_author, param_publication_date, param_publisher, param_ISBN);
		linked_list.add(temp_book);
		return temp_book.getid();
	}
	
	public int add_magazine(String param_title, String param_edition, String param_year, String param_publisher)
	{
		magazine temp_magazine = new magazine(param_title, param_edition, param_year, param_publisher);
		linked_list.add(temp_magazine);
		return temp_magazine.getid();
	}

	public int add_article(String param_title, String param_author, magazine param_content_of, int param_page)
	{
		article temp_article = new article(param_title, param_author, param_content_of, param_page);
		linked_list.add(temp_article);
		return temp_article.getid();
	}

	public boolean remove_product(int id)
	{
		product temp_product;
		for(int i = 0;i < linked_list.size();i++)
		{
			temp_product = linked_list.get(i);
			if(temp_product.getid() == id)
			{
				linked_list.remove(i);
				return true;
			}
		}
		return false;
	}

	public void filter_products(type param_type)
	{
		product temp_product;
		for(int i = 0; i < linked_list.size();i++)
		{
			temp_product = linked_list.get(i);
			if(temp_product.gettype() == param_type)
			{
				System.out.println(temp_product);
			}
		}
	}
	
	public void search_products(String param_title)
	{
		product temp_product;
                for(int i = 0; i < linked_list.size();i++)
                {
                        temp_product = linked_list.get(i);
                        if(temp_product.gettitle().equals(param_title))
                        {
                                System.out.println(temp_product);
                        }
                }
	}
}
