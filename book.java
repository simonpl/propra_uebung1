public class book extends product
{
	private String author;
	private String publication_date;
	private String publisher;
	private String ISBN;

	public book(String param_title, String param_author, String param_publication_date, String param_publisher, String param_ISBN)
	{
		super(param_title, type.BOOK);
		author = param_author;
		publication_date = param_publication_date;
		publisher = param_publisher;
		ISBN = param_ISBN;
	}

	public String toString()
	{
		return "ID: " + id + "; Titel: " + title + "; Autor: " + author + "; Erscheinungsdatum: " + publication_date + "; Verlag: " + publisher + "; ISBN: " + ISBN;
	}
}
